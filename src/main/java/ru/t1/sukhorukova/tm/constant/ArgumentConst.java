package ru.t1.sukhorukova.tm.constant;

public class ArgumentConst {

    public static final String CMD_HELP = "-h";
    public static final String CMD_VERSION = "-v";
    public static final String CMD_ABOUT = "-a";

}
