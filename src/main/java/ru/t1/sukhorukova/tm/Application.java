package ru.t1.sukhorukova.tm;

import ru.t1.sukhorukova.tm.constant.ArgumentConst;
import ru.t1.sukhorukova.tm.constant.CommandConst;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        processArguments(args);
        System.out.println("** WELCOME TASK MANAGER **");
        final Scanner scanner = new Scanner(System.in);
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    private static void processArguments(String[] args) {
        if (args == null || args.length == 0) return;
        processArgument(args[0]);
        System.exit(0);
    }

    private static void processArgument(String command) {
        switch (command) {
            case ArgumentConst.CMD_VERSION:
                showVersion();
                break;
            case ArgumentConst.CMD_ABOUT:
                showAbout();
                break;
            case ArgumentConst.CMD_HELP:
                showHelp();
                break;
            default:
                showArgumentError();
                break;
        }
    }

    private static void processCommand(String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case CommandConst.CMD_VERSION:
                showVersion();
                break;
            case CommandConst.CMD_ABOUT:
                showAbout();
                break;
            case CommandConst.CMD_HELP:
                showHelp();
                break;
            case CommandConst.CMD_EXIT:
                exit();
                break;
            default:
                showCommandError();
                break;
        }
    }

    private static void exit() {
        System.exit(0);
    }

    private static void showArgumentError() {
        System.out.println("[ERROR]");
        System.out.println("This argument not supported...");
        System.exit(1);
    }

    private static void showCommandError() {
        System.out.println("[ERROR]");
        System.out.println("This command not supported...");
        System.exit(1);
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.6.0");
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Victoria Sukhorukova");
        System.out.println("vsukhorukova@t1-consulting.ru");
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s, %s: Show version.\n", CommandConst.CMD_VERSION, ArgumentConst.CMD_VERSION);
        System.out.printf("%s, %s: Show about developer.\n", CommandConst.CMD_ABOUT, ArgumentConst.CMD_ABOUT);
        System.out.printf("%s, %s: Show list arguments.\n", CommandConst.CMD_HELP, ArgumentConst.CMD_HELP);
        System.out.printf("%s: Show list arguments.\n", CommandConst.CMD_EXIT);
    }

}
